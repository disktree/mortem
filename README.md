[![Screenshot](screenshot.png)](https://joshavanier.github.io/mortem/)

**Mortem** ponders upon the inevitable, assuming a life expectancy of ~70 ⊕ years.

```sh
$ npm install mortem
```

```js
var Mortem = require('./mortem');

let d = new Date(); // birth date

Mortem.ndl(d); // number of days lived
Mortem.pro(d); // progress percentage
Mortem.etr(d); // estimated time remaining (days)
Mortem.edd(d); // estimated date of death
```

---

Josh Avanier

**[Twitter](https://twitter.com/joshavanier)** &middot; **[Memex](https://joshavanier.github.io)**

**MIT**
